/*
 * Copyright (c) 2020, Emil Renner Berthing
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */
#include "stdbool.h"
#include "lib/mtimer.h"
#include "lib/eclic.h"
#include "lib/rcu.h"
#include "lib/gpio.h"

#include "LonganNano.h"
#include "bootloader.h"
#include "flash.h"

#define BLINK MTIMER_FREQ /* 1 second */
#define PAGE_WORDS (PAGE_SIZE / sizeof(uint32_t))

void MTIMER_IRQHandler(void)
{
	uint64_t next;

	gpio_pin_toggle(LED_GREEN);

	next = mtimer_mtimecmp() + BLINK;
	MTIMER->mtimecmp_hi = next >> 32;
	MTIMER->mtimecmp_lo = next;
}

/* if the compiler can't generate functions suitable
 * for interrupt handlers, we can't implement this
 * function directly in C
 */
#ifdef __interrupt
void trap_entry(void)
{
	gpio_pin_clear(LED_GREEN);

	while (1)
		/* forever */;
}
#endif

static void mtimer_enable(void)
{
	uint64_t next = mtimer_mtime() + BLINK;

	MTIMER->mtimecmp_hi = next >> 32;
	MTIMER->mtimecmp_lo = next;

	eclic_config(MTIMER_IRQn, ECLIC_ATTR_TRIG_LEVEL, 1);
	eclic_enable(MTIMER_IRQn);
}

static bool page_copy(uint32_t addr, uint32_t dst[PAGE_WORDS], const uint32_t src[PAGE_WORDS])
{
	uint32_t *flash = (uint32_t *)addr;
	bool ret = false;
	unsigned int i;

	for (i = 0; i < PAGE_WORDS; i++) {
		uint32_t word = src[i];

		dst[i] = word;
		if (flash[i] != word)
			ret = true;
	}

	return ret;
}

static int flash_bootloader(void)
{
	uint32_t buf[PAGE_WORDS];
	uint32_t addr = FLASH_BASE;
	const uint32_t *page = bootloader;
	unsigned int i;
	int ret = 0;

	for (i = 0; i < (sizeof(bootloader) / PAGE_SIZE); i++) {
		if (page_copy(addr, buf, page)) {
			ret = flash_page(addr, buf);
			if (ret != 0)
				return ret;
		}
		addr += PAGE_SIZE;
		page += PAGE_WORDS;
	}
	
	return ret;
}

int main(void)
{
	/* initialize system clock */
	rcu_sysclk_init();

	/* initialize eclic */
	eclic_init();
	/* enable global interrupts */
	eclic_global_interrupt_enable();

	/* turn on power to GPIOA and GPIOC */
	RCU->APB2EN |= RCU_APB2EN_PAEN | RCU_APB2EN_PCEN;

	gpio_pin_set(LED_RED);
	gpio_pin_set(LED_GREEN);
	gpio_pin_config(LED_RED,   GPIO_MODE_PP_50MHZ);
	gpio_pin_config(LED_GREEN, GPIO_MODE_PP_50MHZ);

	mtimer_enable();

	if (flash_bootloader())
		gpio_pin_clear(LED_RED);

	while (1)
		wait_for_interrupt();
}
